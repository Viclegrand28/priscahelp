# GL EXAM 
![EXAM ATSA, This is a picture of the exam paper test](./exam.png)


## PROPOSITION DE CORRECTION (SOUS FORME DE COURS)

>
>> 1. *Le processus d'ingenieurie des besoins* : est l'ensembles des iterations ou etapes er
>>> - La Definition du probleme (Comprendre le probleme en terme de besoins)
>>>> - Acquisition (elicitation): déterminer ce que le client demande,
>>>> - Analyse & négociation: comprendre le lien entre les divers besoins du client et combiner ces besoin en un résultat cohérent
>>>> - Spécification: construire une représentation tangible (un document) des besoins,
>>>> - Modélisation du système: construire un ensemble de modèles du système visé qui reflètent les besoins fonctionnels et qui peuvent être évaluée pour sa correction, complétude et consistance,
>>>> - Validation: examiner le document et le modèle pour les évaluer
>>>> - Gestion: identifier, contrôler et tracer les besoins et les modifications qui leur sont apportées
>>> - Le developement technique
>>> - Integration de la solution
>>> - status quo
>> 2. *Les techniques de definition de besoins* :
>> 3. *Classification des besoins* : 
>>> - Besoins très généraux (I): expriment en termes généraux ce que le système doit faire,
>>> - Besoins fonctionnels (II): définissent des aspects du fonctionnement du système,
>>> - Besoins d’implémentation (III): indiquent comment le système doit être implémenté,
>>> - Besoins en performances (IV): établissent des performances minimales pour que le système soit acceptable.
>>>> Ex. Projet « Banque en ligne » :
>>>>> - « Le système doit supporter les transactions sollicitées par les clients »
>>>>> - « L’accès aux comptes et porte-feuilles doit être protégé par un mot de passe »
>>>>> - « Le langage d’implémentation sera C++, pour Windows »
>>>>> - « Le serveur de données doit supporter au moins 250 sessions client simultanées


[Source ressources, Here is the reference of my affirmation](http://www.iro.umontreal.ca/~pift3901/Slides/A03-S02-RE.pdf)